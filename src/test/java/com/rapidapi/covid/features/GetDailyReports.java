package com.rapidapi.covid.features;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/daily_reports",
        glue = "com.rapidapi.covid.steps")
public class GetDailyReports {
}
