package com.rapidapi.covid.steps;

import com.rapidapi.covid.models.DailyCountryDatum;
import com.rapidapi.covid.utils.RestHelper;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.ValidatableResponse;

import java.util.List;

import static com.rapidapi.covid.utils.FileHelper.extractDataFromFile;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class GetDailyReports {

    private String countryName;
    private String countryCode;
    private ValidatableResponse response;
    private String dateFormat;

    @Given("(?:.*) get COVID case in country by name (.*)")
    public void iWantToGetCOVIDCaseInNameCountry(String countryName) {
        this.countryName = countryName;
    }

    @When("(?:.*) searching for specific date (.*)")
    public void iStartSearchingForSpecificDateDate(String date) {
        if (countryCode != null){
            response = RestHelper.getDailyReportByCountryCode(countryCode, date, dateFormat);
        } else {
            response = RestHelper.getDailyReportByCountryName(countryName, date, dateFormat);
        }
    }

    @Then("^I should find next daily report (.*)$")
    public void iShouldFindNextDailyReportResults(String fileName) {
        List<DailyCountryDatum> expected = DailyCountryDatum.fromJsonArray(extractDataFromFile(fileName));
        List<DailyCountryDatum> actual  = DailyCountryDatum.fromJsonArray(response);
        assertThat(actual, equalTo(expected));
    }

    @Given("^I set date format to (.*)$")
    public void iSetDateFormatToDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    @Then("^I should find next status code (.*)$")
    public void iShouldFindNextStatusResult(int code) {
        response.statusCode(code);
    }

    @Given("^I want to get COVID case in country by code (.*)$")
    public void iWantToGetCOVIDCaseInCountryByCodeCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
