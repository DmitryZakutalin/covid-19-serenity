Feature: Check COVID number of cases by country code

  Scenario Outline: Provide information about COVID case in desired country by <code> for specific <date>
    Given I want to get COVID case in country by code <code>
    When I start searching for specific date <date>
    Then I should find next daily report <result>

    Examples:
      | code  | date       | result                        |
      | it    | 2020-04-01 | DailyData_Italy_20200401.json |
      | it    | 2019-04-01 | DailyData_Italy_20190401.json |
      | it    | 2030-04-01 | DailyData_Italy_20300401.json |

  Scenario Outline: Correct codes are returned for different data format (mostly error codes are interesting)
    Given I want to get COVID case in country by code <code>
    And I set date format to <date-format>
    When I start searching for specific date <date>
    Then I should find next status code <result>

    Examples:
      | code  | date       | date-format |result  |
      | it    | 2020-04-01 |  YYYY-MM-DD |200     |
      | it    | 2020-04-01 |  DD-MM-YYYY |400     |
      | it    | DD-MM-YYYY |  DD-MM-YYYY |400     |
      | it    | 2020-04-01 |  2020-04-01 |200     |
      | it    | 2020.04.01 |  YYYY.MM.DD |400     |