Feature: Check COVID number of cases by country name

  Scenario Outline: Provide information about COVID case in desired country by <name> for specific <date>
    Given I want to get COVID case in country by name <name>
    When I start searching for specific date <date>
    Then I should find next daily report <result>

    Examples:
      | name  | date       | result                                                                                    |
      | Italy | 2020-04-01 |  DailyData_Italy_20200401.json |
      | Italy | 2019-04-01 |  DailyData_Italy_20190401.json |
      | Italy | 2030-04-01 |  DailyData_Italy_20300401.json |

  Scenario Outline: Correct codes are returned for different data format (mostly error codes are interesting)
    Given I want to get COVID case in country by name <name>
    And I set date format to <date-format>
    When I start searching for specific date <date>
    Then I should find next status code <result>

    Examples:
      | name  | date       | date-format |result  |
      | Italy | 2020-04-01 |  YYYY-MM-DD |200     |
      | Italy | 2020-04-01 |  DD-MM-YYYY |400     |
      | Italy | DD-MM-YYYY |  DD-MM-YYYY |400     |
      | Italy | 2020-04-01 |  2020-04-01 |200     |
      | Italy | 2020.04.01 |  YYYY.MM.DD |400     |