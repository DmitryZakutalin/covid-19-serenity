package com.rapidapi.covid.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class APICredentials {

    private static APICredentials instance;

    private static final String NOT_REGISTERED_MESSAGE = "To use this API register at https://rapidapi.com";

    private final String appId;
    private final String appKey;

    public static synchronized APICredentials getInstance() {
        if (instance == null) {
            try {
                instance =  new APICredentials(new FileInputStream("rapidapi-covid.properties"));
            } catch (IOException e) {
                throw new CovidException(NOT_REGISTERED_MESSAGE, e);
            }
        }
        return instance;
    }

    private APICredentials(InputStream stream) throws IOException {
        Properties credentialsProperties = new Properties();
        credentialsProperties.load(stream);
        appId = credentialsProperties.getProperty("app_id");
        appKey = credentialsProperties.getProperty("app_key");
    }

    public String getAppKey() {
        return appKey;
    }

    public String getAppId() {
        return appId;
    }
}
