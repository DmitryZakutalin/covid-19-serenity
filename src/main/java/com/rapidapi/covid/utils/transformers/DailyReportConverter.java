package com.rapidapi.covid.utils.transformers;

import com.rapidapi.covid.models.DailyCountryDatum;
import cucumber.api.Transformer;

import java.util.List;

public class DailyReportConverter extends Transformer<List<DailyCountryDatum>> {
    @Override
    public List<DailyCountryDatum> transform(String expectedResult) {
        return DailyCountryDatum.fromJsonArray(expectedResult);
    }
}