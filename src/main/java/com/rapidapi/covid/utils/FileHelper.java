package com.rapidapi.covid.utils;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class FileHelper {


    public static final String TEST_DATA_FOLDER = "test-data/";

    public static String extractDataFromFile(String fileName) {
        try {
            return IOUtils.toString(
                    Thread.currentThread().getContextClassLoader().getResource(TEST_DATA_FOLDER +fileName),
                    StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new CovidException(e);
        }
    }

}
