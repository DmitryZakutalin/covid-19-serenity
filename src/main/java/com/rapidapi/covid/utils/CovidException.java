package com.rapidapi.covid.utils;

import net.serenitybdd.core.exceptions.TestCompromisedException;

public class CovidException extends TestCompromisedException {

    public CovidException(String message, Throwable cause) {
        super(message, cause);
    }

    public CovidException(Throwable e) {
        super(e);
    }
}
