package com.rapidapi.covid.utils;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

import static com.rapidapi.covid.utils.Waiter.rndWait;
import static java.lang.String.format;
import static net.serenitybdd.rest.SerenityRest.then;
import static net.serenitybdd.rest.SerenityRest.with;


public class RestHelper {

    private static final Logger logger = Logger.getLogger(RestHelper.class);
    private static final String APP_ID = APICredentials.getInstance().getAppId();
    private static final String APP_KEY = APICredentials.getInstance().getAppKey();

    private static final String BASE_URL = format("https://%s", APP_ID);

    //    Endpoints
    private static final String DAILY_REPORT_ALL_COUNTRIES = "/report/country/all";
    private static final String DAILY_REPORT_BY_COUNTRY_NAME = "/report/country/name";
    private static final String DAILY_REPORT_BY_COUNTRY_CODE = "/report/country/code";

    static {
        logger.info("Initializing client");
        RequestSpecification spec = new RequestSpecBuilder().build();
        spec.baseUri(BASE_URL);
        spec.headers("x-rapidapi-key", APP_KEY, "x-rapidapi-host", APP_ID);
        SerenityRest.setDefaultRequestSpecification(spec);
    }

    private static void addDateFormat(String dateFormat, Map<String, String> params) {
        if (dateFormat != null){
            params.put("date-format", dateFormat);
        }
    }

    public static ValidatableResponse get(String url, Map <String, ?> params) {
        rndWait();
        with().params(params).get(url);
        ValidatableResponse result = then();
        logger.info(format("GET: %s > %d", url, result.extract().statusCode()));
        return result;
    }


    public static ValidatableResponse getDailyReportByCountryName(String countryName, String date, String dateFormat) {
        Map<String, String> params = new HashMap<>(Map.of("date", date, "name", countryName));
        addDateFormat(dateFormat, params);
        return get(DAILY_REPORT_BY_COUNTRY_NAME, params);
    }


    public static ValidatableResponse getDailyReportByCountryCode(String countryCode, String date, String dateFormat) {
        Map<String, String> params = new HashMap<>(Map.of("date", date, "code", countryCode));
        addDateFormat(dateFormat, params);
        return get(DAILY_REPORT_BY_COUNTRY_CODE, params);
    }

}
