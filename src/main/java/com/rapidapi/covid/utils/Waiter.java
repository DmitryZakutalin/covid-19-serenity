package com.rapidapi.covid.utils;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Waiter {

    public static void waitFor(int mills){
        try {
            TimeUnit.MILLISECONDS.sleep(mills);
        } catch (InterruptedException e) {
            throw new CovidException(e);
        }
    }

    public static void rndWait(){
//        throttling for API forces to  use timeouts
        int timeout  = (new Random().nextInt(5) + 10)*100;
        waitFor(timeout);
    }
}
