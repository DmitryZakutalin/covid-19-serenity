package com.rapidapi.covid.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.restassured.response.ValidatableResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DailyCountryDatum extends BaseCountryDatum {

    private List<ProvinceDatum> provinces;
    private String date;

    /**
     * No args constructor for use in serialization
     *
     */
    public DailyCountryDatum() {
    }

    public static List<DailyCountryDatum> fromJsonArray(ValidatableResponse response) {
        return fromJsonArray(response.extract().asString());
    }

    public static List<DailyCountryDatum> fromJsonArray(String response) {
        return (new Gson()).fromJson(response, new TypeToken<ArrayList<DailyCountryDatum>>() {}.getType());
    }

    public static DailyCountryDatum fromJsonObject(String response) {
        return (new Gson()).fromJson(response, DailyCountryDatum.class);
    }

    public static DailyCountryDatum fromJsonObject(ValidatableResponse response) {
        return (new Gson()).fromJson(response.extract().asString(), DailyCountryDatum.class);
    }

    public DailyCountryDatum(String country, List<ProvinceDatum> provinces, Double latitude, Double longitude, String date) {
        super(country, latitude, longitude);
        this.provinces = provinces;
        this.date = date;
    }


    public List<ProvinceDatum> getProvinces() {
        return provinces;
    }

    public void setProvinces(List<ProvinceDatum> provinces) {
        this.provinces = provinces;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        DailyCountryDatum that = (DailyCountryDatum) o;
        return Objects.equals(provinces, that.provinces) && Objects.equals(date, that.date) && super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), provinces, date);
    }
}