package com.rapidapi.covid.models;

import java.util.Objects;

public class BaseCountryDatum {

    private String country;
    private Double latitude;
    private Double longitude;

    /**
     * No args constructor for use in serialization
     *
     */
    public BaseCountryDatum() {
    }

    public BaseCountryDatum(String country, Double latitude, Double longitude) {
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BaseCountryDatum)) return false;
        BaseCountryDatum that = (BaseCountryDatum) o;
        return Objects.equals(country, that.country) && Objects.equals(latitude, that.latitude)
                && Objects.equals(longitude, that.longitude);
    }

    @Override
    public int hashCode() {
        return Objects.hash(country, latitude, longitude);
    }
}