package com.rapidapi.covid.models;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.restassured.response.ValidatableResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProvinceDatum {

    private String province;
    private Integer confirmed;
    private Integer recovered;
    private Integer deaths;
    private Integer active;

    /**
     * No args constructor for use in serialization
     *
     */
    public ProvinceDatum() {
    }

    public static List<ProvinceDatum> fromJsonArray(ValidatableResponse response) {
        return fromJsonArray(response.extract().asString());
    }

    public static List<ProvinceDatum> fromJsonArray(String response) {
        return (new Gson()).fromJson(response, new TypeToken<ArrayList<ProvinceDatum>>() {}.getType());
    }

    public static ProvinceDatum fromJsonObject(String response) {
        return (new Gson()).fromJson(response, ProvinceDatum.class);
    }

    public static ProvinceDatum fromJsonObject(ValidatableResponse response) {
        return fromJsonObject(response.toString());
    }

    public ProvinceDatum(String province, Integer confirmed, Integer recovered, Integer deaths, Integer active) {
        super();
        this.province = province;
        this.confirmed = confirmed;
        this.recovered = recovered;
        this.deaths = deaths;
        this.active = active;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public Integer getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Integer confirmed) {
        this.confirmed = confirmed;
    }

    public Integer getRecovered() {
        return recovered;
    }

    public void setRecovered(Integer recovered) {
        this.recovered = recovered;
    }

    public Integer getDeaths() {
        return deaths;
    }

    public void setDeaths(Integer deaths) {
        this.deaths = deaths;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProvinceDatum that = (ProvinceDatum) o;
        return Objects.equals(province, that.province) && Objects.equals(confirmed, that.confirmed)
                && Objects.equals(recovered, that.recovered) && Objects.equals(deaths, that.deaths)
                && Objects.equals(active, that.active);
    }

    @Override
    public int hashCode() {
        return Objects.hash(province, confirmed, recovered, deaths, active);
    }
}