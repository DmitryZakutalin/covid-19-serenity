package com.rapidapi.covid.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.inject.internal.asm.$TypeReference;
import io.restassured.response.ValidatableResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class LatestCountryDatum extends BaseCountryDatum {

    private String code;
    private Integer confirmed;
    private Integer recovered;
    private Integer critical;
    private Integer deaths;
    private String lastChange;
    private String lastUpdate;

    /**
     * No args constructor for use in serialization
     *
     */
    public LatestCountryDatum() {
    }

    public static List<LatestCountryDatum> fromJsonArray(ValidatableResponse response) {
        return fromJsonArray(response.toString());
    }

    public static List<LatestCountryDatum> fromJsonArray(String response) {
        return (new Gson()).fromJson(response, new TypeToken<ArrayList<LatestCountryDatum>>() {}.getType());
    }

    public LatestCountryDatum(String country, String code, Integer confirmed, Integer recovered, Integer critical,
                              Integer deaths, Double latitude, Double longitude, String lastChange, String lastUpdate) {
        super(country, latitude, longitude);
        this.code = code;
        this.confirmed = confirmed;
        this.recovered = recovered;
        this.critical = critical;
        this.deaths = deaths;
        this.lastChange = lastChange;
        this.lastUpdate = lastUpdate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Integer confirmed) {
        this.confirmed = confirmed;
    }

    public Integer getRecovered() {
        return recovered;
    }

    public void setRecovered(Integer recovered) {
        this.recovered = recovered;
    }

    public Integer getCritical() {
        return critical;
    }

    public void setCritical(Integer critical) {
        this.critical = critical;
    }

    public Integer getDeaths() {
        return deaths;
    }

    public void setDeaths(Integer deaths) {
        this.deaths = deaths;
    }


    public String getLastChange() {
        return lastChange;
    }

    public void setLastChange(String lastChange) {
        this.lastChange = lastChange;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LatestCountryDatum)) return false;
        LatestCountryDatum that = (LatestCountryDatum) o;
        return Objects.equals(code, that.code) && Objects.equals(confirmed, that.confirmed)
                && Objects.equals(recovered, that.recovered) && Objects.equals(critical, that.critical)
                && Objects.equals(deaths, that.deaths) && Objects.equals(lastChange, that.lastChange)
                && Objects.equals(lastUpdate, that.lastUpdate) && super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, confirmed, recovered, critical, deaths, lastChange, lastUpdate);
    }
}