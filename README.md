# AQA Java test project

Working with Maven and Java 8.

Test project against [COVID-19 data](https://rapidapi.com/Gramzivi/api/covid-19-data). 
To run the tests locally, just run `mvn verify`. Report will be generated in the `target/site/serenity` directory.

Description:
- API server: pick any > https://rapidapi.com/collection/list-of-free-apis
- CI platform: GitLab
- BDD: Cucumber/Gherkin
- Framework: Java Serenity
You need:
- Automate the endpoints inside the CI pipeline to test the service
- Cover positive and negative scenarios
- Write instructions in Readme file on how to install, run and write new tests
- Document the endpoints interactions
- Set Html reporting tool with the test results
- Provide us with a Gitlab link to your repository

Next cases should  be covered:

general:
- invalid API-Key
- invalid date
- invalid date-format

getDailyReportAllCountries
- Check Correct data has beeen returned  for valid (covid exist) date
- Check Correct data has beeen returned  for valid (covid exist) date (not default date-format)
- Check 0 results for before covid  date
- Check 0 results for date in a future covid
- Check error message for invalid date-format

getDailyReportByCountryName
- Check Correct data has beeen returned  for valid (covid exist) date and country
- Check Correct data has beeen returned  for valid (covid exist) date and country (not default date-format)
- Check 0 results for before covid  date and valid country
- Check errormessage for invalid country
- Check 0 results for date in a future covid for valid country
- Check error message for invalid date-format
getDailyReportByCountryCode
- Check Correct data has beeen returned  for valid (covid exist) date and country code
- Check Correct data has beeen returned  for valid (covid exist) date and country code (not default date-format)
- Check 0 results for before covid  date and valid country code
- Check errormessage for invalid country code
- Check 0 results for date in a future covid for valid country code
- Check error message for invalid date-format

For next endpoints data insertion is required to avoid ambiguous and flaky tests
- getLatestAllCountries (same cases as for getDailyReportAllCountries)
- getLatestCountryDataByName (same cases as for getDailyReportByCountryName)
- getDailyReportByCountryName (same cases as for getDailyReportByCountryCode)
